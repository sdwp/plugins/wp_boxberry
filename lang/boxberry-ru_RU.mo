��    $      <  5   \      0     1     F     O     `     q     �     �     �     �     �     �            
     
        *  a   ?     �     �     �     �  	   �     �     �     �     �     �     �          -     E     W     i     |     �  �  �  1   6     h  
   q     |  ?   �  I   �  6     @   M     �  9   �  %   �  /   		  -   9	  &   g	  $   �	  &   �	  �   �	     �
  -   �
  r   �
     >  u   J     �     �     �  �   �  $   o  �   �     "  d   @  S   �     �  �     d   �             $                                           !            #              "                                                 
      	                                                Additional flat rate Boxberry Boxberry API Key Boxberry API Url Boxberry Courier Boxberry Courier Payment After Boxberry Self Boxberry Self Payment After Boxberry Widget Url Boxberry for WooCommerce Default Weight Depth Height Max Weight Min Weight Reception point code The plugin allows you to automatically calculate the shipping cost and create Parsel for Boxberry Title Width autoact boxberry.ru bxbbutton day days days  order_prefix_desc order_prefix_title order_status_send_desc order_status_send_none order_status_send_title ps_on_status_desc ps_on_status_none ps_on_status_title surch Сost Project-Id-Version: Boxberry for WooCommerce 1.0
Report-Msgid-Bugs-To: http://wordpress.org/tag/wordpress
PO-Revision-Date: 2022-03-17 15:56+0500
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 3.0.1
 Добавится к стоимости Boxberry Boxberry API-token* Url для API* Boxberry курьерская с оплатой на сайте Boxberry курьерская с оплатой при получении Boxberry до ПВЗ с оплатой на сайте Boxberry до ПВЗ с оплатой при получении Url для виджета* Плагин доставки Boxberry для Woocommerce Вес по умолчанию (г.)* Максимальная глубина (см.) Максимальная высота (см.) Максимальный вес (г.)* Минимальный вес (г.)* Пункт приема посылок Плагин рассчитывает стоимость доставки до Адреса или ПВЗ, создает отправление в личном кабинете Название Максимальная ширина (см.) Формировать акт автоматически после выгрузки заказа в ЛК Boxberry boxberry.ru Отображать кнопку открытия виджета (иначе, отображается ссылка) день дня дней Укажите префикс, который будет добавляться к номеру заказа при отправке в ЛК Boxberry Префикс для заказов Укажите статус, который будет установлен для заказа, после отправки в ЛК Boxberry Не использовать Автоматический статус, после отправки заказа в ЛК Boxberry Выберите статус для отправки заказа в ЛК Boxberry Не использовать Отправлять заказы в ЛК Boxberry автоматически при наступлении статуса заказа Отключить настройки расчёта для данного вида доставки Стоимость 